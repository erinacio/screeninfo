#-------------------------------------------------
#
# Project created by QtCreator 2019-09-04T20:21:22
#
#-------------------------------------------------

QT       += core gui

TARGET = screeninfo
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
