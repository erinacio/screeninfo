#include <QGuiApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QScreen>
#include <QTextStream>

template <typename T>
QJsonValue toJson(const T &value)
{
    return QJsonValue(value);
}

template <>
QJsonValue toJson<QRect>(const QRect &rect)
{
    QJsonObject obj;
    obj.insert("x", rect.x());
    obj.insert("y", rect.y());
    obj.insert("width", rect.width());
    obj.insert("height", rect.height());
    return obj;
}

template <>
QJsonValue toJson<QRectF>(const QRectF &rect)
{
    QJsonObject obj;
    obj.insert("x", rect.x());
    obj.insert("y", rect.y());
    obj.insert("width", rect.width());
    obj.insert("height", rect.height());
    return obj;
}

template <>
QJsonValue toJson<QSize>(const QSize &size)
{
    QJsonObject obj;
    obj.insert("width", size.width());
    obj.insert("height", size.height());
    return obj;
}

template <>
QJsonValue toJson<QSizeF>(const QSizeF &size)
{
    QJsonObject obj;
    obj.insert("width", size.width());
    obj.insert("height", size.height());
    return obj;
}

template <>
QJsonValue toJson<Qt::ScreenOrientation>(const Qt::ScreenOrientation &o)
{
    switch (o) {
    case Qt::PrimaryOrientation:
        return "primary";
    case Qt::LandscapeOrientation:
        return "landscape";
    case Qt::PortraitOrientation:
        return "portrait";
    case Qt::InvertedLandscapeOrientation:
        return "inverted-landscape";
    case Qt::InvertedPortraitOrientation:
        return "inverted-portrait";
    default:
        return static_cast<int>(o);
    }
}

template <>
QJsonValue toJson<QScreen>(const QScreen &screen)
{
    QJsonObject obj;
    obj.insert("availableGeometry", toJson(screen.availableGeometry()));
    obj.insert("availableSize", toJson(screen.availableSize()));
    obj.insert("availableVirtualGeometry", toJson(screen.availableVirtualGeometry()));
    obj.insert("availableVirtualSize", toJson(screen.availableVirtualSize()));
    obj.insert("depth", toJson(screen.depth()));
    obj.insert("devicePixelRatio", toJson(screen.devicePixelRatio()));
    obj.insert("geometry", toJson(screen.geometry()));
    obj.insert("logicalDotsPerInch", toJson(screen.logicalDotsPerInch()));
    obj.insert("logicalDotsPerInchX", toJson(screen.logicalDotsPerInchX()));
    obj.insert("logicalDotsPerInchY", toJson(screen.logicalDotsPerInchY()));
    obj.insert("manufacturer", toJson(screen.manufacturer()));
    obj.insert("model", toJson(screen.model()));
    obj.insert("name", toJson(screen.name()));
    obj.insert("nativeOrientation", toJson(screen.nativeOrientation()));
    obj.insert("orientation", toJson(screen.orientation()));
    obj.insert("physicalDotsPerInch", toJson(screen.physicalDotsPerInch()));
    obj.insert("physicalDotsPerInchX", toJson(screen.physicalDotsPerInchX()));
    obj.insert("physicalDotsPerInchY", toJson(screen.physicalDotsPerInchY()));
    obj.insert("physicalSize", toJson(screen.physicalSize()));
    obj.insert("primaryOrientation", toJson(screen.primaryOrientation()));
    obj.insert("refreshRate", toJson(screen.refreshRate()));
    obj.insert("serialNumber", toJson(screen.serialNumber()));
    obj.insert("size", toJson(screen.size()));
    obj.insert("virtualGeometry", toJson(screen.virtualGeometry()));
    obj.insert("virtualSize", toJson(screen.virtualSize()));
    return obj;
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QJsonObject obj;
    obj.insert("primary", toJson(*app.primaryScreen()));
    QJsonArray screens;
    for (const QScreen *screen : app.screens()) {
        screens.append(toJson(*screen));
    }
    obj.insert("screens", screens);

    QTextStream(stdout) << QJsonDocument(obj).toJson(QJsonDocument::Indented) << endl;

    return 0;
}
